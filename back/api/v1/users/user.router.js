const { getUsers, register, login, getUsersById, deleteUserById } = require("./user.controller");
const router = require("express").Router();
const { checkToken } = require("../../../auth/token_validation");
const myMulter = require("./../../../middleware/multer");

/** ROUTES */
router.get("/", checkToken, getUsers);
router.post("/register", myMulter.uploadImg.single("file"), register);
router.post("/login", login);
router.delete("/:id", deleteUserById);

router.get("/id/:id", checkToken, getUsersById);

//TODO

module.exports = router;
