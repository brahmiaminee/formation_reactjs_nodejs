const pool = require("../../../config/db");
var moment = require("moment");

module.exports = {
  /* This is a function that is being exported from the module. */
  getUsers: (callback) => {
    pool.query(`SELECT * FROM users`, [], (error, results, fields) => {
      if (error) {
        return callback(error);
      }
      // database records
      console.log(results);
      return callback(null, results);
    });
  },

  /* This is a function that is being exported from the module. */
  getUsersById: (id, callback) => {
    pool.query(`SELECT * FROM users where id = ?`, [id], (error, results, fields) => {
      if (error) {
        return callback(error);
      }
      // database records
      console.log(results);
      return callback(null, results[0]);
    });
  },

  /* This is a function that is being exported from the module. */
  register: (data, callback) => {
    const concatData = {
      ...data,
      created_at: moment(new Date()).format("YYYY-MM-DD, HH:mm:ss"),
    };
    pool.query(
      `insert into users(${Object.keys(concatData).map((key) => key)})
        values(${Object.keys(concatData).map((key) => `'${concatData[key]}'`)})`,

      (error, results, fields) => {
        if (error) {
          return callback(error);
        }
        return callback(null, results);
      }
    );
  },

  /* This is a function that is being exported from the module. */
  login: (email, callback) => {
    pool.query(`SELECT * FROM users where email = ?`, [email], (error, results, fields) => {
      if (error) {
        return callback(error);
      }
      // database records
      console.log(results);
      return callback(null, results[0]);
    });
  },

  /* This is a function that is being exported from the module. */
  deleteUserById: (id, callback) => {
    pool.query(`delete from users where id = ?`, [id], (error, results, fields) => {
      if (error) {
        return callback(error);
      }
      // database records
      console.log(results);
      return callback(null, results);
    });
  },
};
