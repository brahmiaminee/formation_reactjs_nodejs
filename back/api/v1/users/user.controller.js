const { getUsers, register, login, getUsersById, deleteUserById } = require("./user.service");
const { genSaltSync, hashSync, compareSync } = require("bcrypt");
const { sign } = require("jsonwebtoken");

module.exports = {
  /* A function that is being called from the controller. */
  getUsers: (req, res) => {
    getUsers((err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      return res.json(results);
    });
  },

  /* A function that is being called from the controller. */
  register: (req, res) => {
    const body = req.body;
    //req.body ->json
    //form-data ->req.file.path
    //const body = req.body;
    if (!req.file || !req.file.path) {
      console.log(body);
    } else {
      body.file = req.file.path;
    }
    const salt = genSaltSync(10);
    body.password = hashSync(body.password, salt);
    register(body, (err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      return res.json({
        message: "enregistrement ok",
        data: results,
      });
    });
  },

  /* A function that is being called from the controller. */
  login: (req, res) => {
    const email = req.body.email;
    //bd
    login(email, (err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      //true
      const result = compareSync(req.body.password, results.password);
      results.password = undefined;
      const jsontoken = sign({ result: results }, "qwe1234");
      if (result) {
        return res.json({
          success: 1,
          message: "login ok",
          data: results,
          token: jsontoken,
        });
      } else {
        return res.json({
          success: 0,
          message: "email ou mot de passe invalide",
        });
      }
    });
  },

  /* A function that is being called from the controller. */
  getUsersById: (req, res) => {
    const id = req.params.id;
    getUsersById(id, (err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      //true
      return res.json(results);
    });
  },

  /* A function that is being called from the controller. */
  deleteUserById: (req, res) => {
    const id = req.params.id;
    deleteUserById(id, (err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      //true
      return res.json({
        message: "utilisateur est supprimé",
      });
    });
  },
};
