const { getProjects, addProject, getProjectsById, deleteProjectById } = require("./project.controller");
const router = require("express").Router();
const { checkToken } = require("../../../auth/token_validation");
const myMulter = require("./../../../middleware/multer");

/** ROUTES */
router.get("/", checkToken, getProjects);
router.post("/", myMulter.uploadImg.single("file"), addProject);
router.delete("/:id", deleteProjectById);
router.get("/id/:id", checkToken, getProjectsById);

module.exports = router;
