const { getProjects, addProject, getProjectsById, deleteProjectById } = require("./project.service"); //register, login, getUsersById, deleteUserById
// const { genSaltSync, hashSync, compareSync } = require("bcrypt");
// const { sign } = require("jsonwebtoken");

module.exports = {
  /* A function that is being called from the controller.js file. */
  getProjects: (req, res) => {
    getProjects((err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      return res.json(results);
    });
  },

  /* Adding a project to the database. */
  addProject: (req, res) => {
    const body = req.body;
    //req.body ->json
    //form-data ->req.file.path
    //const body = req.body;
    if (!req.file || !req.file.path) {
      console.log(body);
    } else {
      body.file = req.file.path;
    }
    // const salt = genSaltSync(10);
    // body.password = hashSync(body.password, salt);
    addProject(body, (err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      return res.json({
        message: "enregistrement ok",
        data: results,
      });
    });
  },

  /* Getting the id of the project. */
  getProjectsById: (req, res) => {
    const id = req.params.id;
    getProjectsById(id, (err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      //true
      return res.json(results);
    });
  },

  /* Deleting the project by id. */
  deleteProjectById: (req, res) => {
    const id = req.params.id;
    deleteProjectById(id, (err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      //true
      return res.json({
        message: "utilisateur est supprimé",
      });
    });
  },
};
