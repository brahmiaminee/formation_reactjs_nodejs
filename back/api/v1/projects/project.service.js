const pool = require("../../../config/db");
var moment = require("moment");

module.exports = {
  /* A function that is called getProjects. It is a function that is called getProjects.  */
  getProjects: (callback) => {
    pool.query(
      `SELECT *,
    DATE_FORMAT(debut, "%Y-%m-%d") as debut,
    DATE_FORMAT(fin, "%Y-%m-%d") as fin
     FROM projects`,
      [],
      (error, results, fields) => {
        if (error) {
          return callback(error);
        }
        // database records
        console.log(results);
        return callback(null, results);
      }
    );
  },

  /* A function that is called getProjectsById. It is a function that is called getProjectsById. */
  getProjectsById: (id, callback) => {
    pool.query(`SELECT * FROM projects where id = ?`, [id], (error, results, fields) => {
      if (error) {
        return callback(error);
      }
      // database records
      console.log(results);
      return callback(null, results[0]);
    });
  },

  /* A function that is called addProject. */
  addProject: (data, callback) => {
    const concatData = {
      ...data,
      createdAt: moment(new Date()).format("YYYY-MM-DD, HH:mm:ss"),
    };
    pool.query(
      `insert into projects(${Object.keys(concatData).map((key) => key)})
        values(${Object.keys(concatData).map((key) => `'${concatData[key]}'`)})`,

      (error, results, fields) => {
        if (error) {
          return callback(error);
        }
        return callback(null, results);
      }
    );
  },

  /* Deleting a project by id. */
  deleteProjectById: (id, callback) => {
    pool.query(`delete from projects where id = ?`, [id], (error, results, fields) => {
      if (error) {
        return callback(error);
      }
      // database records
      console.log(results);
      return callback(null, results);
    });
  },
};
