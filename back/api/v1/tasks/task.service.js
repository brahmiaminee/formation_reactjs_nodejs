const pool = require("../../../config/db");
var moment = require("moment");
module.exports = {
  //prends callback en function
  //pool.query
  // requst - value - calback function (error, results, fields) => {}
  getTasks: (callback) => {
    pool.query(
      `SELECT *,DATE_FORMAT(debut, "%Y-%m-%d") as debut,
        DATE_FORMAT(fin, "%Y-%m-%d") as fin
        FROM tasks`,
      [],
      (error, results, fields) => {
        if (error) {
          //l erreur
          console.log("🚀 ~ file: task.service.js ~ line 16 ~ error", error);
          return callback(error);
        }
        // resultat
        console.log("🚀 ~ file: task.service.js ~ line 22 ~ results", results);
        return callback(null, results);
      }
    );
  },

  /* A function that is getting the task by id. */
  getTaskById: (id, callback) => {
    pool.query(`SELECT * FROM tasks where id = ?`, [id], (error, results, fields) => {
      if (error) {
        return callback(error);
      }
      // database records
      console.log(results);
      return callback(null, results[0]);
    });
  },

  /* Adding a task to the database. */
  addTask: (data, callback) => {
    const concatData = {
      ...data,
      createdAt: moment(new Date()).format("YYYY-MM-DD, HH:mm:ss"),
    };
    pool.query(
      `insert into tasks(${Object.keys(concatData).map((key) => key)})
        values(${Object.keys(concatData).map((key) => `'${concatData[key]}'`)})`,

      (error, results, fields) => {
        if (error) {
          return callback(error);
        }
        return callback(null, results);
      }
    );
  },

  /* Deleting the task by id. */
  deleteTaskById: (id, callback) => {
    pool.query(`delete from tasks where id = ?`, [id], (error, results, fields) => {
      if (error) {
        return callback(error);
      }
      // database records
      console.log(results);
      return callback(null, results);
    });
  },
};
