const { getTasks, addTask, getTaskById, deleteTaskById } = require("./task.controller");
const router = require("express").Router();
const { checkToken } = require("../../../auth/token_validation");
const myMulter = require("./../../../middleware/multer");

/** Routes */
router.get("/", checkToken, getTasks);
router.post("/", myMulter.uploadImg.single("file"), checkToken, addTask);
router.delete("/:id", checkToken, deleteTaskById);
router.get("/id/:id", checkToken, getTaskById);

module.exports = router;
