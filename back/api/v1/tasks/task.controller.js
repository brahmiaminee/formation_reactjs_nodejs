const { getTasks, addTask, getTaskById, deleteTaskById } = require("./task.service");

module.exports = {
  /*  */
  getTasks: (req, res) => {
    //res -> incomming message
    //res server response
    //appeler la methode callback
    getTasks((err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      return res.json(results);
    });
  },

  /* A function that adds a task to the database. */
  addTask: (req, res) => {
    const body = req.body;
    addTask(body, (err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      return res.json({
        message: "enregistrement ok pour la tâche",
        data: results,
      });
    });
  },

  /* A function that is getting the task by id. */
  getTaskById: (req, res) => {
    const id = req.params.id;
    getTaskById(id, (err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      return res.json(results);
    });
  },

  /* Deleting the task by id. */
  deleteTaskById: (req, res) => {
    const id = req.params.id;
    deleteTaskById(id, (err, results) => {
      if (err) {
        console.log(err);
        return;
      }
      return res.json({
        message: "utilisateur est supprimé",
      });
    });
  },
};
