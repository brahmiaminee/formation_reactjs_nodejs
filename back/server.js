const express = require("express");
const cors = require("cors");
require("dotenv").config();
const app = express();
const pool = require("./config/db");

/** CORS */
var corsOptions = {
  origin: "http://localhost:3000",
};
app.use(cors(corsOptions));

/** parser to json */
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/** ROUTES */
const userRouter = require("./api/v1/users/user.router");
app.use("/api/v1", userRouter);
const taskRouter = require("./api/v1/tasks/task.router");
app.use("/api/v1/tasks", taskRouter);
const projetRouter = require("./api/v1/projects/project.router");
app.use("/api/v1/projects", projetRouter);

/** Test Route */
app.get("/", (req, res) => {
  res.json({ message: "Welcome to  tutu." });
});

app.use(express.static(__dirname + "/uploads")); // you can access image from url
app.use("/uploads", express.static(__dirname + "/uploads/"));

//null undefined false
//3001
const PORT = process.env.APP_PORT || 3000;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});
