const { verify } = require("jsonwebtoken");

module.exports = {
  /* A middleware function that checks if the token is valid or not. */
  checkToken: (req, res, next) => {
    let token = req.get("authorization");
    if (token) {
      token = token.slice(7);
      verify(token, "qwe1234", (err, decoded) => {
        if (err) {
          res.json({
            success: 0,
            message: "token invalid",
          });
        } else {
          next();
        }
      });
    } else {
      res.json({
        success: 0,
        message: "acces denied ! user not autorized",
      });
    }
  },
};
