require("dotenv").config();
const util = require("util");
const mysql = require("mysql");

/* Creating a connection pool to the database. */
const pool = mysql.createPool({
  connectionLimit: 10,
  host: process.env.DB_HOST,
  user: process.env.DB_USER,
  password: process.env.DB_PASS,
  database: process.env.MYSQL_DB,
});

// Ping database to check for common exception errors.
pool.getConnection((err, connection) => {
  if (err) {
    if (err.code === "PROTOCOL_CONNECTION_LOST") {
      console.error("Database connection was closed.");
    }
    if (err.code === "ER_CON_COUNT_ERROR") {
      console.error("Database has too many connections.");
    }
    if (err.code === "ECONNREFUSED") {
      console.error("Database connection was refused.");
    }
  }
  if (connection) {
    console.log("connection to MYSQL");
    connection.release();
  } else {
    console.log(err);
  }
  return;
});
// Promisify for Node.js async/await.
pool.query = util.promisify(pool.query);
module.exports = pool;
