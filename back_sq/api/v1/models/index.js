const Sequelize = require("sequelize");
const sequelize = require("../../../config/db.config");
const db = {};
db.Sequelize = Sequelize;
db.sequelize = sequelize;
//tutorials est le nom de la table BD
db.users = require("../users/user.model")(sequelize, Sequelize);
module.exports = db;
