const { genSaltSync, hashSync, compareSync } = require("bcrypt");
const { sign } = require("jsonwebtoken");
const db = require("../models");
const User = db.users;
const Op = db.Sequelize.Op;
// Create and Save a new User
exports.register = (req, res) => {
  const salt = genSaltSync(10);

  const user = {
    nom: req.body.nom,
    prenom: req.body.prenom,
    email: req.body.email,
    password: hashSync(req.body.password, salt),
    image: req?.file?.path,
  };

  // Save User in the database
  User.create(user)
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the User.",
      });
    });
};
exports.login = (req, res) => {
  const email = req.body.email;
  var condition = email ? { email: { [Op.like]: `%${email}%` } } : null;
  User.findAll({ where: condition })
    .then((data) => {
      const result = compareSync(req.body.password, data[0].password);
      const jsontoken = sign({ result: data[0] }, "qwe1234");
      //res.send(data);
      if (result) {
        data[0].password == undefined;
        res.json({ data: data, token: jsontoken });
      } else {
        res.json({
          message: "email or password invalide",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving users.",
      });
    });
};
// Retrieve all Users from the database.
exports.findAll = (req, res) => {
  //   db.sequelize.query("SELECT * FROM `users`").then((data) => {
  //     res.send(data[0][0]);
  //   });
  User.findAll()
    .then((data) => {
      res.send(data);
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error occurred while retrieving users.",
      });
    });
};
// Find a single User with an id
exports.findOne = (req, res) => {
  //   db.sequelize.query("SELECT * FROM `users` where id = '1'").then((data) => {
  //     res.send(data[0][0]);
  //   });
  const id = req.params.id;
  User.findByPk(id)
    .then((data) => {
      if (data) {
        res.send(data);
      } else {
        res.status(404).send({
          message: `Cannot find User with id=${id}.`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error retrieving User with id=" + id,
      });
    });
};
// Update a User by the id in the request
exports.update = (req, res) => {
  const id = req.body.id;
  const user = { ...req.body, image: req?.file?.path };
  User.update(user, {
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "User was updated successfully.",
        });
      } else {
        res.send({
          message: `Cannot update User with id=${id}. Maybe User was not found or req.body is empty!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Error updating User with id=" + id,
      });
    });
};
// Delete a User with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;
  User.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          message: "User was deleted successfully!",
        });
      } else {
        res.send({
          message: `Cannot delete User with id=${id}. Maybe User was not found!`,
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        message: "Could not delete User with id=" + id,
      });
    });
};
// Delete all Users from the database.
exports.deleteAll = (req, res) => {};
// Find all published Users
exports.findAllPublished = (req, res) => {};
