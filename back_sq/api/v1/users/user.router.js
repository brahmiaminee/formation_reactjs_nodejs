module.exports = (app) => {
  const users = require("./user.controller");
  var router = require("express").Router();
  const { checkToken } = require("../../../auth/token_validation");
  const myMulter = require("../../../middleware/multer");
  // Create a new Tutorial
  router.post("/register", myMulter.uploadImg.single("image"), users.register);
  router.post("/login", users.login);
  // Retrieve all users
  router.get("/", checkToken, users.findAll);
  //   // Retrieve all published users
  //   router.get("/published", users.findAllPublished);
  //   // Retrieve a single Tutorial with id
  router.get("/:id", users.findOne);
  //   // Update a Tutorial with id
  router.put("/", myMulter.uploadImg.single("image"), users.update);
  // Delete a Tutorial with id
  router.delete("/:id", users.delete);
  //   // Delete all users
  //   router.delete("/", users.deleteAll);

  app.use("/api/v1/users", router);
};
