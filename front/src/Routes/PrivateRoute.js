import React from "react";
import { Route, Redirect } from "react-router-dom";
import { isLogin } from "../Utils/SesionUtils";

/**
 * If the user is logged in, render the component, otherwise redirect to the login page
 * @returns A Route component with a render prop that returns a Layout component with a Component component as a child.
 */
function PrivateRoute({ component: Component, layout: Layout, ...rest }) {
  return <Route {...rest} render={(props) => <Layout>{isLogin() ? <Component {...props}></Component> : <Redirect to="/login" />}</Layout>}></Route>;
}

export default PrivateRoute;
