import { Route } from "react-router-dom";

/**
 * It takes a component and a layout as props, and returns a Route component that renders the layout and the component.
 * @returns A Route component with a render prop that returns a Layout component with a Component component as a child.
 */
function AppRoute({ component: Component, layout: Layout, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => (
        <Layout>
          <Component {...props}></Component>
        </Layout>
      )}
    ></Route>
  );
}
export default AppRoute;
