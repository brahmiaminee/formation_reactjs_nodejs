import React from "react";
import { Redirect, Route } from "react-router";
import { isLogin } from "../Utils/SesionUtils";

/**
 * If the user is logged in, redirect to the home page, otherwise render the component
 * @returns A Route component with a render prop that returns a Layout component with a child component that is either a Redirect component or a
 * Component component.
 */
function PublicRoute({ component: Component, layout: Layout, ...rest }) {
  return <Route {...rest} render={(props) => <Layout>{isLogin() ? <Redirect to="/" /> : <Component {...props}></Component>}</Layout>}></Route>;
}

export default PublicRoute;
