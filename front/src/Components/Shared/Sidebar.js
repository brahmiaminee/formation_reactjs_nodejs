import React from "react";
import { Link } from "react-router-dom";

export default function Sidebar() {
  return (
    <div className="dlabnav">
      <div className="dlabnav-scroll">
        <ul className="metismenu" id="menu">
          <li>
            <a className="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
              <i className="flaticon-381-networking" />
              <span className="nav-text">Projets</span>
            </a>
            <ul aria-expanded="false">
              <li>
                <Link to="/projects">Liste des projets</Link>
              </li>
              <li>
                <Link to="/addproject">Ajouter projet</Link>
              </li>
            </ul>
          </li>
          <li>
            <a className="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
              <i className="flaticon-381-television" />
              <span className="nav-text">Tâches</span>
            </a>
            <ul aria-expanded="false">
              <li>
                <Link to="/tasks">Liste des tâches</Link>
              </li>

              <li>
                <Link to="/addtask">Ajouter tâche</Link>
              </li>
            </ul>
          </li>

          {/* <li>
            <a className="has-arrow ai-icon" href="javascript:void()" aria-expanded="false">
              <i className="flaticon-381-controls-3" />
              <span className="nav-text">Charts</span>
            </a>
            <ul aria-expanded="false">
              <li>
                <a href="chart-flot.html">Flot</a>
              </li>
              <li>
                <a href="chart-morris.html">Morris</a>
              </li>
              <li>
                <a href="chart-chartjs.html">Chartjs</a>
              </li>
              <li>
                <a href="chart-chartist.html">Chartist</a>
              </li>
              <li>
                <a href="chart-sparkline.html">Sparkline</a>
              </li>
              <li>
                <a href="chart-peity.html">Peity</a>
              </li>
            </ul>
          </li> */}
        </ul>
        <a className="add-menu-sidebar d-block" href="javascript:void(0)" data-toggle="modal" data-target="#addOrderModalside">
          + New Project
        </a>
        <div className="copyright">
          <p>
            <strong>Vora Saas Admin Dashboard</strong> © 2021 All Rights Reserved
          </p>
          <p>
            Made with <span className="heart" /> by DexignLab
          </p>
        </div>
      </div>
    </div>
  );
}
