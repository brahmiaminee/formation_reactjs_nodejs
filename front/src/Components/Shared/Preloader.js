import React from "react";

/**
 * It returns a div with an id of preloader, which contains a div with a class of sk-three-bounce, which contains three divs with a class of sk-child
 * sk-bounce1, sk-child sk-bounce2, and sk-child sk-bounce3
 * @returns A div with a class of sk-three-bounce and three divs with a class of sk-child sk-bounce1, sk-child sk-bounce2, and sk-child sk-bounce3.
 */
export default function Preloader() {
  return (
    <div id="preloader">
      <div className="sk-three-bounce">
        <div className="sk-child sk-bounce1" />
        <div className="sk-child sk-bounce2" />
        <div className="sk-child sk-bounce3" />
      </div>
    </div>
  );
}
