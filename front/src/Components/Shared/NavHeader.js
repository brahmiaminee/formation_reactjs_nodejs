import React from "react";

/**
 * It returns a div with a link and a div
 * @returns A div with a class of nav-header.
 */
function NavHeader() {
  return (
    <div className="nav-header">
      <a href="index-2.html" className="brand-logo">
        <img className="logo-abbr" src="images/logo.png" alt />
        <img className="logo-compact" src="images/logo-text.png" alt />
        <img className="brand-title" src="images/logo-text.png" alt />
      </a>
      <div className="nav-control">
        <div className="hamburger">
          <span className="line" />
          <span className="line" />
          <span className="line" />
        </div>
      </div>
    </div>
  );
}

export default NavHeader;
