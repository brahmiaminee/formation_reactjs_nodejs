import React from "react";

/**
 * It returns a div with a className of footer, which contains a div with a className of copyright, which contains a paragraph with a link to my LinkedIn
 * profile
 * @returns A div with a class of footer. Inside the div is another div with a class of copyright. Inside the copyright div is a paragraph. Inside the
 * paragraph is a link.
 */
function Footer() {
  return (
    <div className="footer">
      <div className="copyright">
        <p>
          Copyright © Designed &amp; Developed by{" "}
          <a href="https://www.linkedin.com/in/modibo-bamba-73326a182/" target="_blank">
            Madibo Bamba
          </a>{" "}
          2021
        </p>
      </div>
    </div>
  );
}

export default Footer;
