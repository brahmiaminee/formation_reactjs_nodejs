import React, { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { getTasksById } from "../../Services/TaskApi";

function EditTask(props) {
  const history = useHistory();
  const [object, setObjet] = useState(null);
  const [description, setdescription] = useState(null);
  const [debut, setdebut] = useState(null);
  const [fin, setfin] = useState(null);
  const [status, setstatus] = useState(null);

  useEffect(() => {
    //alert(props.match.params.id);
    getTasksById(props.match.params.id).then((res) => {
      console.log(res.data);
      const result = res.data;
      setObjet(result.objet);
      setdescription(result.description);
      setdebut(result.debut);
      setfin(result.fin);
      setstatus(result.status);
    });
  }, []);

  return (
    <>
      <div className="form-group">
        <label htmlFor="exampleInputEmail1">Objet</label>
        <input
          type="text"
          className="form-control"
          id="exampleInputEmail1"
          aria-describedby="emailHelp"
          placeholder="Enter objet"
          value={object}
          onChange={(e) => setObjet(e.target.value)}
        />
      </div>
      <div className="form-group">
        <label htmlFor="exampleInputPassword1">Description</label>
        <input
          type="text"
          className="form-control"
          id="exampleInputPassword1"
          placeholder="description"
          value={description}
          onChange={(e) => setdescription(e.target.value)}
        />
      </div>

      <div className="form-group">
        <label htmlFor="exampleInputPassword1">Date début</label>
        <input
          type="date"
          className="form-control"
          id="exampleInputPassword1"
          placeholder="date début"
          value={debut}
          onChange={(e) => setdebut(e.target.value)}
        />
      </div>

      <div className="form-group">
        <label htmlFor="exampleInputPassword1">Date fin</label>
        <input
          type="date"
          className="form-control"
          id="exampleInputPassword1"
          placeholder="date fin"
          value={fin}
          onChange={(e) => setfin(e.target.value)}
        />
      </div>

      <div className="form-group">
        <label className="mb-1">
          <strong>Status task</strong>
        </label>
        <select className="form-control" id="exampleFormControlSelect1" onChange={(e) => setstatus(e.target.value)}>
          <option>Choisir un status</option>
          <option value="encours">En cours</option>
          <option value="termine">Terminée</option>
        </select>
      </div>

      <button type="submit" className="btn btn-primary">
        Ajouter
      </button>
    </>
  );
}

export default EditTask;
