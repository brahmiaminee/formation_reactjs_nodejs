import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { deleteTaskById, getTasks } from "../../Services/TaskApi";

function ListTasks() {
  const [data, setdata] = useState(null);

  useEffect(() => {
    getDataFromApi();
  }, []);

  const getDataFromApi = () => {
    getTasks().then((res) => {
      setdata(res.data);
    });
  };

  const handleDelete = (id) => {
    //alert(id);
    deleteTaskById(id).then((res) => {
      alert("projet suprimer");
      getDataFromApi();
    });
  };

  return (
    <div className="col-lg-12">
      <div className="card">
        <div className="card-header">
          <h4 className="card-title">La liste des tâches</h4>
        </div>
        <div className="card-body">
          <div className="table-responsive">
            <table className="table table-responsive-md">
              <thead>
                <tr>
                  <th>
                    <strong>#ID.</strong>
                  </th>
                  <th>
                    <strong>Objet</strong>
                  </th>
                  <th>
                    <strong>Description</strong>
                  </th>
                  <th>
                    <strong>Date début</strong>
                  </th>
                  <th>
                    <strong>Date Fin</strong>
                  </th>
                  <th>
                    <strong>Status</strong>
                  </th>
                </tr>
              </thead>
              <tbody>
                {data &&
                  data.map((el) => (
                    <tr>
                      <td>
                        <strong>{el.id}</strong>
                      </td>
                      {/* <td>
                      <div className="d-flex align-items-center">
                        <img src="images/avatar/1.jpg" className="rounded-lg mr-2" width={24} alt />
                        <span className="w-space-no">Dr. Jackson</span>
                      </div>
                    </td> */}

                      <td>{el.objet}</td>
                      <td>{el.description}</td>
                      <td>{el.debut}</td>
                      <td>{el.fin}</td>
                      <td>
                        {el.status == "encours" && (
                          <div className="d-flex align-items-center">
                            <i className="fa fa-circle text-warning mr-1" /> {el.status}
                          </div>
                        )}

                        {el.status == "termine" && (
                          <div className="d-flex align-items-center">
                            <i className="fa fa-circle text-success mr-1" /> {el.status}
                          </div>
                        )}
                      </td>
                      <td>
                        <div className="d-flex">
                          <Link to={"/editproject/" + el.id} className="btn btn-primary shadow btn-xs sharp mr-1">
                            <i className="fa fa-pencil" />
                          </Link>
                          <Link to="#" className="btn btn-danger shadow btn-xs sharp" onClick={() => handleDelete(el.id)}>
                            <i className="fa fa-trash" />
                          </Link>
                        </div>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ListTasks;
