import React, { useEffect, useState } from "react";
import { deleteProjectById, getProjets } from "../../Services/ProjectApi";
import { Link } from "react-router-dom";

function ListProjects() {
  const [data, setdata] = useState(null);

  //chargement
  useEffect(() => {
    getDataFromApi();
  }, []);

  const getDataFromApi = () => {
    getProjets().then((res) => {
      setdata(res.data);
    });
  };

  const handleDelete = (id) => {
    //alert(id);
    deleteProjectById(id).then((res) => {
      alert("projet suprimer");
      getDataFromApi();
    });
  };

  return (
    <div className="col-lg-12">
      <div className="card">
        <div className="card-header">
          <h4 className="card-title">La liste des projets</h4>
        </div>
        <div className="card-body">
          <div className="table-responsive">
            <table className="table table-responsive-md">
              <thead>
                <tr>
                  <th className="width80">
                    <strong>#Id</strong>
                  </th>
                  <th className="width80">
                    <strong>Nom</strong>
                  </th>
                  <th>
                    <strong>Description</strong>
                  </th>
                  <th>
                    <strong>Début</strong>
                  </th>
                  <th>
                    <strong>Fin</strong>
                  </th>
                  <th>
                    <strong>Status</strong>
                  </th>
                  <th />
                </tr>
              </thead>
              <tbody>
                {data &&
                  data.map((el) => (
                    <tr>
                      <td>
                        <strong>{el.id}</strong>
                      </td>
                      <td>{el.nom}</td>
                      <td>{el.description}</td>
                      <td>{el.debut}</td>
                      <td>{el.fin}</td>

                      {/* danger / warning / primary / default / success */}
                      {/* rouge /  jaune / blue / gre / vert */}

                      <td>
                        {el.status == "encours" && <span className="badge light badge-warning">En cours</span>}

                        {el.status == "termine" && <span className="badge light badge-success">Terminé</span>}
                      </td>

                      <td>
                        <div className="dropdown">
                          <button type="button" className="btn btn-default light sharp" data-toggle="dropdown">
                            <svg width="20px" height="20px" viewBox="0 0 24 24" version="1.1">
                              <g stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                                <rect x={0} y={0} width={24} height={24} />
                                <circle fill="#000000" cx={5} cy={12} r={2} />
                                <circle fill="#000000" cx={12} cy={12} r={2} />
                                <circle fill="#000000" cx={19} cy={12} r={2} />
                              </g>
                            </svg>
                          </button>
                          <div className="dropdown-menu">
                            <a className="dropdown-item" href="#">
                              Edit
                            </a>
                            <Link to="#" className="dropdown-item" onClick={() => handleDelete(el.id)}>
                              Delete
                            </Link>
                          </div>
                        </div>
                      </td>
                    </tr>
                  ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ListProjects;
