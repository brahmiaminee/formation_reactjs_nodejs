import axios from "axios";
import { getSession } from "../Utils/SesionUtils";

/* Setting the header for the axios request. */
const config = {
  headers: { Authorization: `Bearer ${getSession("token")}` },
};

/**
 * It sends a POST request to the server with the form data
 * @param nom - nom,
 * @param prenom - "test"
 * @param email - "test@test.com"
 * @param password - "123456"
 * @param role - "user"
 * @param file - the file to upload
 * @returns The response object.
 */
export async function inscription(nom, prenom, email, password, role, file) {
  // const body = { nom: nom, prenom: prenom, email: email, password: password, role: role, isactiv: "1" };

  const frmData = new FormData();
  frmData.append("nom", nom);
  frmData.append("prenom", prenom);
  frmData.append("email", email);
  frmData.append("password", password);
  frmData.append("role", role);
  frmData.append("isactiv", "1");
  frmData.append("file", file);

  const httpHeaders = {
    "Content-Type": "multipart/form-data",
  };

  const options = {
    headers: httpHeaders,
  };

  try {
    const response = await axios.post("http://localhost:3001/api/v1/register", frmData, options);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * It takes an email and password, and sends a POST request to the server with the email and password as the body.
 *
 * If the request is successful, it returns the response. If the request is unsuccessful, it logs the error to the console.
 * @param email - email,
 * @param password - "password"
 * @returns The response object from the server.
 */
export async function login(email, password) {
  const body = { email: email, password: password };
  try {
    const response = await axios.post("http://localhost:3001/api/v1/login", body);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * It gets the user's id from the session storage and then uses that id to get the user's information from the database.
 * @returns The response object from the axios request.
 */
export async function getUserById() {
  try {
    const response = await axios.get("http://localhost:3001/api/v1/id/" + getSession("id"), config);
    return response;
  } catch (error) {
    console.error(error);
  }
}
