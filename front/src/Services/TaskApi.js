import axios from "axios";
import { getSession } from "../Utils/SesionUtils";

/* It's a constant that contains the headers for the request. */
const config = {
  headers: { Authorization: `Bearer ${getSession("token")}` },
};

/**
 * It sends a POST request to the server with the form data
 * @param nom - nom,
 * @param prenom - "test",
 * @param email - "test@test.com"
 * @param password - "123456"
 * @param role - "admin"
 * @param file - The file to upload
 * @returns The response from the server.
 */
// export async function addNewTask(nom, prenom, email, password, role, file) {
//   // const body = { nom: nom, prenom: prenom, email: email, password: password, role: role, isactiv: "1" };

//   const frmData = new FormData();
//   frmData.append("nom", nom);
//   frmData.append("prenom", prenom);
//   frmData.append("email", email);
//   frmData.append("password", password);
//   frmData.append("role", role);
//   frmData.append("isactiv", "1");
//   frmData.append("file", file);

//   const httpHeaders = {
//     "Content-Type": "multipart/form-data",
//   };

//   const options = {
//     headers: httpHeaders,
//   };

//   try {
//     const response = await axios.post("http://localhost:3001/api/v1/tasks", frmData, options);
//     return response;
//   } catch (error) {
//     console.error(error);
//   }
// }

/**
 * It's an async function that uses the axios library to make a GET request to the server, and returns the response.
 * @returns The response object from the axios request.
 */
export async function getTasks() {
  try {
    const response = await axios.get("http://localhost:3001/api/v1/tasks", config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function getTasksById(id) {
  try {
    const response = await axios.get("http://localhost:3001/api/v1/tasks/id/" + id, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function deleteTaskById(id) {
  try {
    const response = await axios.delete("http://localhost:3001/api/v1/tasks/" + id, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function addNewTask(objet, description, debut, fin, status) {
  const body = { objet: objet, description: description, debut: debut, fin: fin, status: status };
  try {
    const response = await axios.post("http://localhost:3001/api/v1/tasks", body, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}
