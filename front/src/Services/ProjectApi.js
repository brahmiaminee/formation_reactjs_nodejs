import axios from "axios";
import { getSession } from "../Utils/SesionUtils";

/* It's a configuration object that is used to send the token in the header of the request. */
const config = {
  headers: { Authorization: `Bearer ${getSession("token")}` },
};

/**
 * It takes a file and some other data, and sends it to the server.
 * </code>
 * @param nom - nom,
 * @param prenom - "test"
 * @param email - "test@test.com"
 * @param password - "123456"
 * @param role - "admin"
 * @param file - The file to upload
 * @returns The response from the server.
 */
export async function addNewProject(nom, prenom, email, password, role, file) {
  // const body = { nom: nom, prenom: prenom, email: email, password: password, role: role, isactiv: "1" };

  const frmData = new FormData();
  frmData.append("nom", nom);
  frmData.append("prenom", prenom);
  frmData.append("email", email);
  frmData.append("password", password);
  frmData.append("role", role);
  frmData.append("isactiv", "1");
  frmData.append("file", file);

  const httpHeaders = {
    "Content-Type": "multipart/form-data",
  };

  const options = {
    headers: httpHeaders,
  };

  try {
    const response = await axios.post("http://localhost:3001/api/v1/projects", frmData, options);
    return response;
  } catch (error) {
    console.error(error);
  }
}

/**
 * It's an async function that makes a GET request to the server, and returns the response.
 * @returns The response object.
 */
export async function getProjets() {
  try {
    const response = await axios.get("http://localhost:3001/api/v1/projects", config);
    return response;
  } catch (error) {
    console.error(error);
  }
}

export async function deleteProjectById(id) {
  try {
    const response = await axios.delete("http://localhost:3001/api/v1/projects/" + id, config);
    return response;
  } catch (error) {
    console.error(error);
  }
}
