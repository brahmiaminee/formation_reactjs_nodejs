import React from "react";
import Footer from "../Components/Shared/Footer";
import Header from "../Components/Shared/Header";
import NavHeader from "../Components/Shared/NavHeader";
import Preloader from "../Components/Shared/Preloader";
import Sidebar from "../Components/Shared/Sidebar";

//dashboard
/**
 * @param props - {
 * @returns The LayoutDashboard component is being returned.
 */
function LayoutDashboard(props) {
  return (
    <div>
      {/* <Preloader /> */}
      {/* add classe show to hide preloader */}
      <div id="main-wrapper" className="show">
        <NavHeader />
        <Header />
        <Sidebar />
        <div className="content-body">
          <div className="container-fluid">
            <React.Fragment>{props.children}</React.Fragment>
          </div>
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default LayoutDashboard;
