import React from "react";

// page login et inscription
/**
 * It returns a React Fragment that contains the children of the component.
 * @param props - This is the object that contains the properties that are passed to the component.
 * @returns A React Fragment.
 */
function LayoutVide(props) {
  return <React.Fragment>{props.children}</React.Fragment>;
}

export default LayoutVide;
