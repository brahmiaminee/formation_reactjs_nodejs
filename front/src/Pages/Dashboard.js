import React from "react";
import Main from "../Components/Main/Main";

/**
 * It returns a Main component.
 * @returns Main component
 */
function Dashboard() {
  return <Main></Main>;
}

export default Dashboard;
