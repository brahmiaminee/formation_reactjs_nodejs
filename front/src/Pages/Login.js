import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { login } from "../Services/UserApi";

function Login() {
  //const navigate = useNavigate();
  const history = useHistory();
  const [email, setEmail] = useState(null);
  const [password, setpassword] = useState(null);

  /**
   * If the email and password are not null, then login, and if the login is successful, then save the user's details in local storage and redirect to the
   * dashboard.
   */
  const handleLogin = () => {
    if (email != null || password != null) {
      login(email, password).then((res) => {
        //vérifier si l'email et mot pass incorrect
        const result = res.data.data;
        const token = res.data.token;
        const success = res.data.success;
        if (success == "0") {
          alert("email ou mot de passe invalide");
        } else {
          //garder les details de utilisateur dans local storage
          localStorage.setItem("email", result.email);
          localStorage.setItem("id", result.id);
          localStorage.setItem("nom", result.nom);
          localStorage.setItem("prenom", result.prenom);
          localStorage.setItem("isconnected", true);
          localStorage.setItem("token", token);
          //redirection vers deashboard
          history.push("/");
        }
      });
    } else {
      alert("veuillez remplir tous les champs");
    }
  };

  return (
    <div className="authincation h-100" style={{ marginTop: 60 }}>
      <div className="container h-100">
        <div className="row justify-content-center h-100 align-items-center">
          <div className="col-md-6">
            <div className="authincation-content">
              <div className="row no-gutters">
                <div className="col-xl-12">
                  <div className="auth-form">
                    <div className="text-center mb-3">
                      <a href="index-2.html">
                        <img src="images/logo-full.png" alt />
                      </a>
                    </div>
                    <h4 className="text-center mb-4 text-white">Se connecter à votre compte</h4>

                    <div className="form-group">
                      <label className="mb-1 text-white">
                        <strong>Email</strong>
                      </label>
                      <input type="email" className="form-control" value={email} onChange={(e) => setEmail(e.target.value)} />
                    </div>
                    <div className="form-group">
                      <label className="mb-1 text-white">
                        <strong>Password</strong>
                      </label>
                      <input type="password" className="form-control" value={password} onChange={(e) => setpassword(e.target.value)} />
                    </div>
                    <div className="form-row d-flex justify-content-between mt-4 mb-2"></div>
                    <div className="text-center">
                      <button onClick={handleLogin} type="submit" className="btn bg-white text-primary btn-block">
                        Se connecter
                      </button>
                    </div>

                    <div className="new-account mt-3">
                      <p className="text-white">
                        Vous n'avez pas de compte?{" "}
                        <Link to="/inscription" className="text-white">
                          Inscription
                        </Link>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
