import React, { useState } from "react";
import { Link, useHistory } from "react-router-dom";
import { inscription } from "../Services/UserApi";

function Inscription() {
  //const navigate = useNavigate();
  const history = useHistory();
  const [nom, setNom] = useState(null);
  const [prenom, setPrenom] = useState(null);
  const [email, setEmail] = useState(null);
  const [password, setPassword] = useState(null);
  const [role, setRole] = useState(null);
  const [imageUrl, setImageUrl] = useState("null");
  const [imageName, setImageName] = useState("Choisir photo");

  /**
   * If the user has entered a value for all the fields, then call the inscription function and navigate
   * to the login page.
   *
   * If the user has not entered a value for all the fields, then display an alert.
   */
  const handleInscription = () => {
    if (nom != null || prenom != null || email != null || password != null || role != null) {
      inscription(nom, prenom, email, password, role, imageUrl).then((res) => {
        console.log(res);
        //message succès
        //alert("inscription éffectué avec succès");
        //redirction vers loginnnnnnn
        history.push("/login");
      });
    } else {
      alert("veuillez remplir tous les champs");
    }
  };

  /**
   * It takes the file that was uploaded, and sets the state of the imageName and imageUrl to the file that was uploaded.
   * @param e - the event object
   */
  const handleUploadImage = (e) => {
    console.log(e.target.files);
    setImageName(e.target.files[0].name);
    setImageUrl(e.target.files[0]);
  };

  return (
    <div className="authincation h-100" style={{ marginTop: 80 }}>
      <div className="container h-100">
        <div className="row justify-content-center h-100 align-items-center">
          <div className="col-md-6">
            <div className="authincation-content">
              <div className="row no-gutters">
                <div className="col-xl-12">
                  <div className="auth-form">
                    <div className="text-center mb-3">
                      <a href="index-2.html">
                        <img src="images/logo-full.png" alt />
                      </a>
                    </div>
                    <h4 className="text-center mb-4 text-white">Se conecter à votre compte</h4>

                    <div className="form-group">
                      <label className="mb-1 text-white">
                        <strong>Nom</strong>
                      </label>
                      <input value={nom} type="text" className="form-control" placeholder="nom" onChange={(e) => setNom(e.target.value)} />
                    </div>
                    <div className="form-group">
                      <label className="mb-1 text-white">
                        <strong>Prénom</strong>
                      </label>
                      <input value={prenom} type="text" className="form-control" placeholder="prenom" onChange={(e) => setPrenom(e.target.value)} />
                    </div>
                    <div className="form-group">
                      <label className="mb-1 text-white">
                        <strong>Email</strong>
                      </label>
                      <input
                        value={email}
                        type="email"
                        className="form-control"
                        placeholder="hello@example.com"
                        onChange={(e) => setEmail(e.target.value)}
                      />
                    </div>
                    <div className="form-group">
                      <label className="mb-1 text-white">
                        <strong>Mote de passe</strong>
                      </label>
                      <input value={password} type="password" className="form-control" onChange={(e) => setPassword(e.target.value)} />
                    </div>

                    <div className="form-group">
                      <label className="mb-1 text-white">
                        <strong>Rôle utilisateur</strong>
                      </label>
                      <select className="form-control" id="exampleFormControlSelect1" onChange={(e) => setRole(e.target.value)}>
                        <option>Choisir un rôle</option>
                        <option value="user">Simple utilisateur</option>
                        <option value="admin">Administrateur</option>
                        <option value="moderator">Moderateur</option>
                      </select>
                    </div>

                    <div className="input-group mb-3 mt-4">
                      <div className="input-group-prepend">
                        <span className="input-group-text">Télécharger</span>
                      </div>
                      <div className="custom-file">
                        <input type="file" className="custom-file-input" onChange={(e) => handleUploadImage(e)} />
                        <label className="custom-file-label">{imageName}</label>
                      </div>
                    </div>

                    <div className="text-center mt-4">
                      <button type="submit" onClick={handleInscription} className="btn bg-white text-primary btn-block">
                        Inscription
                      </button>
                    </div>

                    <div className="new-account mt-3">
                      <p className="text-white">
                        Vous avez un compte?{" "}
                        <Link to="/login" className="text-white">
                          Se connecter
                        </Link>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Inscription;
