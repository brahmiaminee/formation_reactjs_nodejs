import Inscription from "./Pages/Inscription";
import Login from "./Pages/Login";
import { BrowserRouter, Switch } from "react-router-dom";
import LayoutVide from "./Layout/LayoutVide";
import LayoutDashboard from "./Layout/LayoutDashboard";
import Main from "./Components/Main/Main";
import ListProjects from "./Components/Projects/ListProjects";
import AddProjct from "./Components/Projects/AddProjct";
import ListTasks from "./Components/Tasks/ListTasks";
import AddTask from "./Components/Tasks/AddTask";
import PrivateRoute from "./Routes/PrivateRoute";
import PublicRoute from "./Routes/PublicRoute";
import EditTask from "./Components/Tasks/EditTask";

/**
 * It returns a div with the text "test" inside of it
 * @returns A div with the text "test"
 */
function App() {
  return (
    <BrowserRouter>
      <Switch>
        <PublicRoute exact path="/login" layout={LayoutVide} component={Login} />
        <PublicRoute exact path="/inscription" layout={LayoutVide} component={Inscription} />
        <PrivateRoute exact path="/" layout={LayoutDashboard} component={Main} />
        <PrivateRoute exact path="/projects" layout={LayoutDashboard} component={ListProjects} />
        <PrivateRoute exact path="/addproject" layout={LayoutDashboard} component={AddProjct} />
        <PrivateRoute exact path="/tasks" layout={LayoutDashboard} component={ListTasks} />
        <PrivateRoute exact path="/addtask" layout={LayoutDashboard} component={AddTask} />
        <PrivateRoute exact path="/editproject/:id" layout={LayoutDashboard} component={EditTask} />
      </Switch>
    </BrowserRouter>
  );
}

export default App;
